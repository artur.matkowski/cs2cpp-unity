using UnityEditor;
using System.Runtime.InteropServices;
using UnityEngine;
using System;

public static class callbackStringW
{

    public delegate void CallbackDelegateWString(IntPtr arg, UInt32 size);
    [DllImport("unmanaged_dll", CharSet = CharSet.Unicode)]
    public static extern void setCallbackWString(CallbackDelegateWString aCallback);


    static char[] buff = null;
    static string b;

    static public void CallbackWString(IntPtr arg, UInt32 size)
    {
        if(buff==null)
        {
            buff = new char[size];
        }
        else if(buff.Length < size)
        {
            buff = new char[size];
        }

        Marshal.Copy(arg, buff, 0, (int)size);

        
        b = string.Create((int)size, buff, (chars, buf) =>
        {
            for (int i = 0; i < chars.Length; i++) chars[i] = buf[i];
        }
        );

        
        Debug.Log("callbackWString called. " + b);
    }

    [MenuItem("cpp to c# by delegate/CallbackWString(IntPtr arg, UInt32 size)")]
    public static void InvokeCString()
    {
        setCallbackWString(new CallbackDelegateWString(CallbackWString));
        Debug.Log("Calling passed C++ callback...\n");
    }
}
