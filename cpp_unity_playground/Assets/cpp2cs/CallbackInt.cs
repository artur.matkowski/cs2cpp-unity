using UnityEditor;
using System.Runtime.InteropServices;
using UnityEngine;
using System;

public static class CallbackInt
{
    public delegate void CallbackDelegateInt(int arg);
    [DllImport("unmanaged_dll")]
    public static extern void setCallbackInt(CallbackDelegateInt aCallback);

    static private CallbackDelegateInt del;
    static public void callback(int arg)
    {
        Debug.Log("void callback(int arg) called. " + arg.ToString());
    }

    [MenuItem("cpp to c# by delegate/CallbackDelegateInt(int arg)")]
    public static void InvokeC()
    {
        del = new CallbackDelegateInt(callback);
        setCallbackInt(del);
        Debug.Log("Calling passed C++ callback...\n");
    }

}
