using UnityEditor;
using System.Runtime.InteropServices;
using UnityEngine;

public static class Callback
{
    public delegate void CallbackDelegate();
    [DllImport("unmanaged_dll")]
    public static extern void setCallback(CallbackDelegate aCallback);

    static private CallbackDelegate del;
    static public void callback()
    {
        Debug.Log("void callback() called. ");
    }

    [MenuItem("cpp to c# by delegate/CallbackDelegate()")]
    public static void InvokeC()
    {
        del = new CallbackDelegate(callback);
        setCallback(del);
        Debug.Log("Calling passed C++ callback...\n");
    }

}
