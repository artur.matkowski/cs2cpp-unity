using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEditor;
using UnityEngine;

public static class sortIntArr
{
    // The imported function
    [DllImport("unmanaged_dll", EntryPoint = "TestSort")]
    public static extern void TestSort(int[] a, int length);




    [MenuItem("c# to cpp/TestSort(int[] a, int length)")]
    public static void InvokeC()
    {
        int[] tab = { 9, 2, 6, 3, 7 };

        Debug.Log("Unsorted tab[]: " + string.Join(", ", tab));

        TestSort(tab, tab.Length);

        Debug.Log("Calling passed C++ callback... Sorted tab[]: " + string.Join(", ", tab));
    }
}
