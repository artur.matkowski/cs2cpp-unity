using System;
using System.Runtime.InteropServices;
using System.Text;
using UnityEditor;
using UnityEngine;

public static class FetchString
{
    [DllImport("unmanaged_dll", CharSet = CharSet.Ansi)]
    public static extern void fetchString([MarshalAs(UnmanagedType.LPStr)] StringBuilder version, ref UInt32 size);


    [MenuItem("c# to cpp/fetchString([MarshalAs(UnmanagedType.LPStr)] StringBuilder version, ref UInt32 size)")]
    public static void InvokeC()
    {
        UInt32 size = 512;
        var sb = new StringBuilder((int)size);
        fetchString(sb, ref size);
        string version = sb.ToString();
        Debug.Log("Calling passed C++ callback..." + version);
    }

}
