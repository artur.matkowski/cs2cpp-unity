#include "callback.h"

extern "C" {

    void setCallback(CallbackFunction delegate)
    {
        delegate();
    }
}