
extern "C" {
    typedef void (*CallbackFunctionWString)(wchar_t* str, int size);
    __declspec(dllexport)  void setCallbackWString(CallbackFunctionWString delegate);
}