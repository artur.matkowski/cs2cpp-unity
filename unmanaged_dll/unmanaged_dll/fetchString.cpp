#include "fetchString.h"
#include <algorithm>
#include <wchar.h>

extern "C" {

    void fetchString(char* buffer, unsigned long* pSize)
    {
        if (pSize == nullptr)
        {
            return;
        }

        static const char* version = ">string from cpp<";
        unsigned long size = strlen(version) + 1;
        if ((buffer != nullptr) && (*pSize >= size))
        {
            strcpy_s(buffer, size, version);
        }

        *pSize = size;
    }
}